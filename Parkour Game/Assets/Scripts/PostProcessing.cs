using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine;

public class PostProcessing : MonoBehaviour
{
    private Bloom bloom = null;

    // Start is called before the first frame update
    void Start()
    {
        Volume volume = GetComponent<Volume>();

        volume.sharedProfile.TryGet<Bloom>(out bloom);

        if (bloom != null)
        {
            bloom.intensity.value = 1.0f;
        }
    }
}
