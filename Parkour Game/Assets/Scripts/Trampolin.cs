using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampolin : MonoBehaviour
{
    public float trampoVel = 10f;
    GameObject Jugador;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Jugador")
        {
            Jugador = collision.gameObject;
            Salto();

        }
    }

    private void Salto()
    {
        Rigidbody rb = Jugador.GetComponent<Rigidbody>();
        rb.AddForce(Jugador.transform.up * trampoVel, ForceMode.Impulse);
    }
}
