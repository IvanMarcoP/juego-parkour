using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escalar : MonoBehaviour
{
    [Header("Datos")]
    public float open = 100f;
    public float rango = 1f;
    public bool tocarPared = false;
    public float velVertical;
    public Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Disparar();

        if(Input.GetKey("w") && tocarPared == true)
        {
            transform.position += Vector3.up * Time.deltaTime * velVertical;
            GetComponent<Rigidbody>().isKinematic = true;
            tocarPared = false;
            GetComponent<Rigidbody>().isKinematic = false;
        }

        if(Input.GetKeyUp("w"))
        {
            GetComponent<Rigidbody>().isKinematic = false;
            tocarPared=false;
        }
    }

    private void Disparar()
    {
        RaycastHit hit;
        if(Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, rango))
        {
            Target target = hit.transform.GetComponent<Target>();
            if (target != null)
            {
                tocarPared = true;
            }
        }
    }
    
}
