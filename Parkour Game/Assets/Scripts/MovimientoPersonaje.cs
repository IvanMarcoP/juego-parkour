using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPersonaje : MonoBehaviour
{
    private Rigidbody rb;
    //CapsuleCollider collider;

    [Header("Movimiento")]
    public float caminarVelocidad;
    public float SprintVelocidad;
    public float GrapplingVelocidad;
    private float rapidezDesplazamiento = 10f;

    [Header("Agacharse")]
    public float VelAgachado;
    public float escalaAgachadoY;
    private float startEscalaY;

    [Header("Salto")]
    public float fuerzaSalto;
    public float saltoCD;
    //public float multiAire;
   
    [Header("Sliding")]
    public float slideVelocidad;

    [Header("Dash")]
    public float dashVelocidad;
    //public GameObject efectoDash;
    bool puedeDash;


    [Header("Textos")]
    public TMPro.TMP_Text textoObjetosRecolectados;
    public TMPro.TMP_Text textoGanaste;
    private int cont;
    //public float WallRunVelocidad;
    //public float wallRunTiempo;
    //float tiempoOriginal;
    //bool wallrun;


    [Header("Teclas de Acceso")]
    public KeyCode Saltar = KeyCode.Space;
    public KeyCode Sprint = KeyCode.LeftShift;
    public KeyCode Crouch = KeyCode.LeftControl;
    public KeyCode Slide = KeyCode.Q;
    public KeyCode Dash = KeyCode.E;

    [Header("Estados")]
    public float distanciaPiso = 0.7f;
    public float gravedad;
    public LayerMask capaPiso;
    public Camera camaraPrimeraPersona;
    public ParticleSystem particulas;
    public bool piso;
    bool puedeSalto;
    public bool grappling;

    // Start is called before the first frame update
    void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("musica");

        rb = GetComponent<Rigidbody>();
        //collider = GetComponent<CapsuleCollider>();

        textoGanaste.text = "";
        cont = 0;
        seteoTextos();

        Physics.gravity *= gravedad;
        startEscalaY = transform.localScale.y;

        //tiempoOriginal = wallRunTiempo;

        puedeSalto = true;
    }

    private void FixedUpdate()
    {
        if (puedeDash)
        {
            Dashing();
        }
    }

    private void seteoTextos()
    {
        textoObjetosRecolectados.text = "Cantidad recolectada: " + cont.ToString() + " / 6";
        if(cont == 6)
        {
            textoGanaste.text = "Has Ganado";
        }

    }

    // Update is called once per frame
    void Update()
    {
        Movimiento();
        MisInputs();

        
        //piso = Physics.Raycast(transform.position, Vector3.down, distanciaPiso * 0.5f + 0.3f, capaPiso);

    }

    private void MisInputs()
    {
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetKey(Saltar))
        {
            if (EstaEnPiso() && puedeSalto)
            {

                puedeSalto = false;

                Salto();

                Invoke(nameof(ResetSalto), saltoCD);
            }
        }

        if (Input.GetKeyDown(Slide))
        {
            Sliding();
        }
        else if (Input.GetKeyUp(Slide))
        {
            LevantarseSlide();
        }

        if (Input.GetKeyDown(Sprint))
        {
            rapidezDesplazamiento = SprintVelocidad;
        }

        if (Input.GetKeyUp(Sprint))
        {
            rapidezDesplazamiento = caminarVelocidad;
        }

        if (grappling)
        {
            rapidezDesplazamiento = GrapplingVelocidad;
        }
        else if (!grappling)
        {
            rapidezDesplazamiento = caminarVelocidad;
        }

        if (Input.GetKeyDown(Crouch))
        {
            transform.localScale = new Vector3(transform.localScale.x, escalaAgachadoY, transform.localScale.z);
            rb.AddForce(Vector3.down * 1.5f, ForceMode.Impulse);
            rapidezDesplazamiento = VelAgachado;
        }

        if (Input.GetKeyUp(Crouch))
        {
            transform.localScale = new Vector3(transform.localScale.x, startEscalaY, transform.localScale.z);
            rapidezDesplazamiento = caminarVelocidad;
        }

        if (Input.GetKeyDown(Dash))
        {
            puedeDash = true;
            //Dashing();
        }
    }

    private bool EstaEnPiso()
    {
        return Physics.Raycast(transform.position, Vector3.down, distanciaPiso);
    }

    private void Movimiento()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
    }

    private void Sliding()
    {
        transform.localScale = new Vector3(transform.localScale.x, escalaAgachadoY, transform.localScale.z);
        rb.AddForce(transform.forward * slideVelocidad, ForceMode.VelocityChange);
    }

    private void LevantarseSlide()
    {
        transform.localScale = new Vector3(transform.localScale.x, startEscalaY, transform.localScale.z);
    }

    private void Salto()
    {
        //rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

        rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);

    }

    private void ResetSalto()
    {
        puedeSalto = true;
    }

    private void Dashing()
    {
        rb.AddForce(transform.forward * dashVelocidad, ForceMode.Impulse);
        puedeDash = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coleccionable") == true)
        {
            cont += 1;
            seteoTextos();
            GestorDeAudio.instancia.ReproducirSonido("moneda");
            Instantiate(particulas, transform.position, Quaternion.identity);
            other.gameObject.SetActive(false);
        }
    }

}
