using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallRunning : MonoBehaviour
{
    [Header("Datos")]
    public float distanciaPared;
    public float saltoMinimo;
    public float gravedadWallRun;
    public float fuerzaWallRun;
    public LayerMask queEsPiso;
    public LayerMask queEsPared;

    bool paredIzq;
    bool paredDer;

    private Rigidbody rb;

    RaycastHit derechaHit;
    RaycastHit izquierdaHit;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        chequeoPared();

        if(puedeWallRun())
        {
            if(paredIzq)
            {
                empezarWallRun();
            }
            else if (paredDer)
            {
                empezarWallRun();
            }
            else
            {
                detenerWallRun();
            }
        }
        else 
        { 
            detenerWallRun();
        }
    }

    void chequeoPared()
    {
        paredIzq = Physics.Raycast(transform.position, -Vector3.right, out izquierdaHit, distanciaPared, queEsPared);

        paredDer = Physics.Raycast(transform.position, Vector3.right, out derechaHit, distanciaPared, queEsPared);
    }

    private bool puedeWallRun()
    {
        return !Physics.Raycast(transform.position, Vector3.down, saltoMinimo, queEsPiso);
    }

    void empezarWallRun()
    {
        rb.useGravity = false;

        rb.AddForce(Vector3.down * gravedadWallRun, ForceMode.Force);

        if(Input.GetKeyDown(KeyCode.Space))
        {
            if (paredIzq)
            {
                Vector3 wallRunDireccionSalto = transform.up + izquierdaHit.normal;
                rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
                rb.AddForce(wallRunDireccionSalto * fuerzaWallRun * 50, ForceMode.Force);
            }else if (paredDer)
            {
                Vector3 wallRunDireccionSalto = transform.up + derechaHit.normal;
                rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
                rb.AddForce(wallRunDireccionSalto * fuerzaWallRun * 50, ForceMode.Force);
            }
        }
    }

    void detenerWallRun()
    {
        rb.useGravity = true;
    }
}
