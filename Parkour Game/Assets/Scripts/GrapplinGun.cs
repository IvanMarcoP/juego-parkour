using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplinGun : MonoBehaviour
{
    [Header("Datos")]
    public LayerMask queSeEngancha;
    public Transform puntaArma, camara, jugador;
    public KeyCode Swing = KeyCode.Mouse1;
    public MovimientoPersonaje mj;

    //public float fuerzaEnfrente;
    //public float extenderCableVel;

    private Rigidbody rb;
    private LineRenderer lr;
    private Vector3 grapplePoint;
    private float distanciaMaxima = 100f;
    private SpringJoint joint;


    private void Awake()
    {
        lr = GetComponent<LineRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        //movimientoAire();

        if (Input.GetKeyDown(Swing))
        {
            empezarGrapple();
        }

        if (Input.GetKeyUp(Swing))
        {
            detenerGrapple();
        }
    }

    void LateUpdate()
    {
        dibujarDisparo();
    }

    //void movimientoAire()
    //{
    //    if (Input.GetKeyDown(KeyCode.Space)) 
    //    {
    //        Vector3 direccionAlPunto = grapplePoint - transform.position;
    //        rb.AddForce(direccionAlPunto.normalized * fuerzaEnfrente * Time.deltaTime);

    //        float distanciaAlpunto = Vector3.Distance(transform.position, grapplePoint);

    //        joint.maxDistance = distanciaAlpunto * 0.8f;
    //        joint.minDistance = distanciaAlpunto * 0.25f;

    //    }

    //    //if(Input.GetKey(KeyCode.S))
    //    //{
    //    //    float extenderDelPunto = Vector3.Distance(transform.position, grapplePoint) * extenderCableVel;

    //    //    joint.maxDistance = extenderDelPunto * 0.8f;
    //    //    joint.minDistance = extenderDelPunto * 0.25f;
    //    //}
    //}

    void empezarGrapple()
    {
        mj.grappling = true;

        RaycastHit hit;
        if (Physics.Raycast(camara.position, camara.forward, out hit, distanciaMaxima, queSeEngancha))
        {
            grapplePoint = hit.point;
            joint = jugador.gameObject.AddComponent<SpringJoint>();
            joint.autoConfigureConnectedAnchor = false;
            joint.connectedAnchor = grapplePoint;

            float distanciaDelPunto = Vector3.Distance(jugador.position, grapplePoint);

            joint.maxDistance = distanciaDelPunto * 0.8f;
            joint.minDistance = distanciaDelPunto * 0.25f;

            joint.spring = 4.5f;
            joint.damper = 7f;
            joint.massScale = 4.5f;

            lr.positionCount = 2;
        }
    }

    void dibujarDisparo()
    {
        if (!joint)
        {
            return;
        }
        lr.SetPosition(0, puntaArma.position);
        lr.SetPosition(1, grapplePoint);

    }

    void detenerGrapple()
    {
        mj.grappling = false;

        lr.positionCount = 0;
        Destroy(joint);
    }
}
